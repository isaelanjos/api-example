var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');

var app = express();

app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());

app.use(function(req, res, next){
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "content-type");
  res.setHeader("Content-Type", "application/json");
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.listen(9090, function(){ console.log('Server port: 9090') });

app.get('/api', function(req, res){
  fs.readFile('users.json', 'utf8', function(err, data){
    if (err) {
      var response = {status: 'error', resultado: err};
      res.json(response);
    } else {
      var obj = JSON.parse(data);
      var result = 'Nenhum usuário foi encontrado';

      obj.users.forEach(function(users) {
        if (users != null) {
          if (users.id == req.query.id) {
            result = users;
          }
        }
      });

      var response = {status: 'successful', resultado: result};
      res.json(response);
    }
  });
});

app.post('/api', function(req, res){
  fs.readFile('users.json', 'utf8', function(err, data){
    if (err) {
      var response = {status: 'error', resultado: err};
      res.json(response);
    } else {
      var obj = JSON.parse(data);
      req.body.id = obj.users.length + 1;

      obj.users.push(req.body);

      fs.writeFile('users.json', JSON.stringify(obj), function(err) {
        if (err) {
          var response = {status: 'error', resultado: err};
          res.json(response);
        } else {
          var response = {status: 'successful', resultado: 'Successful'};
          res.json(response);
        }
      });
    }
  });
});

app.put('/api', function(req, res){
  fs.readFile('users.json', 'utf8', function(err, data){
    if (err) {
      var response = {status: 'error', resultado: err};
      res.json(response);
    } else {
      var obj = JSON.parse(data);

      obj.users[(req.body.id - 1)].name = req.body.name;
      obj.users[(req.body.id - 1)].document = req.body.document;

      fs.writeFile('users.json', JSON.stringify(obj), function(err) {
        if (err) {
          var response = {status: 'error', resultado: err};
          res.json(response);
        } else {
          var response = {status: 'successful', resultado: 'Successful'};
          res.json(response);
        }
      });
    }
  });
});

app.delete('/api', function(req, res){
  fs.readFile('users.json', 'utf8', function(err, data){
    if (err) {
      var response = {status: 'error', resultado: err};
      res.json(response);
    } else {
      var obj = JSON.parse(data);

      delete obj.users[(req.body.id - 1)];

      fs.writeFile('users.json', JSON.stringify(obj), function(err) {
        if (err) {
          var response = {status: 'error', resultado: err};
          res.json(response);
        } else {
          var response = {status: 'successful', resultado: 'Successful'};
          res.json(response);
        }
      });
    }
  });
});
